import fetch from 'sync-fetch';

class BladeOneRenderer {
    static #config = {
        host: 'http://localhost',
        port: '81'
    }

    static setup(config) {
        this.#config = {
            ...this.#config,
            ...config,
        }
        return this.#config;
    }

    static render(view,data = null) {
        const metadata = fetch(this.#config.host + ':' + this.#config.port + '/?view=' + view + '&data=' + JSON.stringify(data),{}).text();
        return metadata;
    }
}

export {BladeOneRenderer};
export default BladeOneRenderer;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.BladeOneRenderer = void 0;

var _syncFetch = _interopRequireDefault(require("sync-fetch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classStaticPrivateFieldSpecGet(receiver, classConstructor, descriptor) { _classCheckPrivateStaticAccess(receiver, classConstructor); _classCheckPrivateStaticFieldDescriptor(descriptor, "get"); return _classApplyDescriptorGet(receiver, descriptor); }

function _classApplyDescriptorGet(receiver, descriptor) { if (descriptor.get) { return descriptor.get.call(receiver); } return descriptor.value; }

function _classStaticPrivateFieldSpecSet(receiver, classConstructor, descriptor, value) { _classCheckPrivateStaticAccess(receiver, classConstructor); _classCheckPrivateStaticFieldDescriptor(descriptor, "set"); _classApplyDescriptorSet(receiver, descriptor, value); return value; }

function _classCheckPrivateStaticFieldDescriptor(descriptor, action) { if (descriptor === undefined) { throw new TypeError("attempted to " + action + " private static field before its declaration"); } }

function _classCheckPrivateStaticAccess(receiver, classConstructor) { if (receiver !== classConstructor) { throw new TypeError("Private static access of wrong provenance"); } }

function _classApplyDescriptorSet(receiver, descriptor, value) { if (descriptor.set) { descriptor.set.call(receiver, value); } else { if (!descriptor.writable) { throw new TypeError("attempted to set read only private field"); } descriptor.value = value; } }

class BladeOneRenderer {
  static setup(config) {
    _classStaticPrivateFieldSpecSet(this, BladeOneRenderer, _config, _objectSpread(_objectSpread({}, _classStaticPrivateFieldSpecGet(this, BladeOneRenderer, _config)), config));

    return _classStaticPrivateFieldSpecGet(this, BladeOneRenderer, _config);
  }

  static render(view, data = null) {
    const metadata = (0, _syncFetch.default)(_classStaticPrivateFieldSpecGet(this, BladeOneRenderer, _config).host + ':' + _classStaticPrivateFieldSpecGet(this, BladeOneRenderer, _config).port + '/?view=' + view + '&data=' + JSON.stringify(data), {}).text();
    return metadata;
  }

}

exports.BladeOneRenderer = BladeOneRenderer;
var _config = {
  writable: true,
  value: {
    host: 'http://localhost',
    port: '81'
  }
};
var _default = BladeOneRenderer;
exports.default = _default;